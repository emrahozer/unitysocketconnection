﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestConnection : MonoBehaviour
{
    public InputField Host;
    public InputField Port;
    public Button ConnectButton;
    public Button SendButton;
    public InputField DataInput;
    public Text Status;
    private SocketServer _socketServer;

    // Use this for initialization
    void Start()
    {
        Host.text = "192.168.0.18";
        Port.text = "9000";

        SendButton.gameObject.SetActive(false);
        Status.gameObject.SetActive(false);
        DataInput.gameObject.SetActive(false);

        ConnectButton.onClick.AddListener(OnConnect);
        SendButton.onClick.AddListener(OnSend);
        _socketServer = new SocketServer();

    }

    private void OnConnect()
    {
        _socketServer.Host = Host.text;
        _socketServer.Port = int.Parse(Port.text);
        try
        {
            _socketServer.SetupSocket();
        }
        catch (Exception exception)
        {
            SetStatus(exception.Message);
            throw;
        }

        if (_socketServer.socketReady)
        {
            SetStatus("Socket Ready");

            Host.gameObject.SetActive(false);
            Port.gameObject.SetActive(false);
            ConnectButton.gameObject.GetComponentInChildren<Text>().text = "Disconnect";
            ConnectButton.onClick.RemoveAllListeners();
            ConnectButton.onClick.AddListener(OnDisconnect);

            DataInput.gameObject.SetActive(true);
            SendButton.gameObject.SetActive(true);
        }
    }

    private void OnDisconnect()
    {
        _socketServer.CloseSocket();
        Host.gameObject.SetActive(true);
        Port.gameObject.SetActive(true);
        ConnectButton.gameObject.GetComponentInChildren<Text>().text = "Connect";
        ConnectButton.onClick.RemoveAllListeners();
        ConnectButton.onClick.AddListener(OnConnect);

        DataInput.gameObject.SetActive(false);
        SendButton.gameObject.SetActive(false);
    }

    private void SetStatus(string message)
    {
        if(!Status.gameObject.activeSelf)
            Status.gameObject.SetActive(true);
        Status.text = message;
    }

    public void OnSend()
    {
        _socketServer.WriteSocket(DataInput.text);
    }
}
